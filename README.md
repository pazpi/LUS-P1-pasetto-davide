1.  crea il numero dei simboli di uscita
    ./POS-count.sh file-with-word-and-symbols > POS.count

2.  crea il numero delle parole in entrata
    ./TOK_POS-count.sh file-with-word-and-symbols > TOK.count
    
3.  crea la probabilità di ogni parola e simbolo
    ./TOKEN_POS-calc-prob.sh TOK.count POS.count > TOK_POS.probs

4.  crea lexicon, due colonne, insiede delle parole e dei simboli con affianco numero incrementale
    ./create-lexicon.sh TOK_POS.probs POS.count > lexicon.lex

5.  crea il file .txt per la fst, inserendo antecedentemente lo stato di entrata (0) e lo stato di uscita (0)
    ./create-text-0-0.sh TOK_POS.probs > TOK_POS_0_0.probs

6.  crea txt per la  fst con i simboli e unknown
    ./fst-txt-unknown.sh POS.txt > unknown.txt

7.  crea txt per la fst con la stringa di parole in ingresso
    ./fst-input-phrase.sh "Stringa di parole" > phrase.txt

8.  compila la fst con tutti i simboli e le parole, quella dei simboli e unknown e della frase in ingresso
    ./FST-compile.sh lexicon.lex TOK_POS_0_0.probs

9.  unione della fst generale e della fst-unknown
    fstun

10. composizione della fst della frase di partenza e della fst unione



# Note

Unione fst fst-unknown > union.fst
Compose str.fst union.fst > compose.fst
 
fstdraw lexicon.lex compose.fst
