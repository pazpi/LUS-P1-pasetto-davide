#!/usr/bin/env bash

IN_SYM=$1
OUT_SYM=$2

usage(){
    echo "Usage: $0 input_symbols output_symbols"
}

if [ -z $IN_SYM ] && [ -z $OUT_SYM ]; then
    usage
else
    state=1
    printf "<unknown>\t0\n"
    while IFS=$' \t\n' read -r sym other; do
        echo -e $sym'\t'$state
        ((state++))
    done < $IN_SYM
    while IFS=$' \t\n' read -r sym other; do
        echo -e $sym'\t'$state
        ((state++))
    done < $OUT_SYM
fi

echo -e <unk>'\t'$state
