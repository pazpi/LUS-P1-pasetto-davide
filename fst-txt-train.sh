#!/usr/bin/env bash

INPUT=$1

state=0

while IFS=$' \t\n' read -r token pos prob; do
    echo -e $state'\t'$state'\t'$token'\t'$pos'\t'$prob
done < $INPUT

echo 0
