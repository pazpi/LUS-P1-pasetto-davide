#!/usr/bin/env bash


usage-txt(){
    echo "Usage: $0 Lexicon TXT";
}

usage-fst(){
    echo "Usage: $0 Lexicon FST";
}

select todo in compile printing drawing all close;
do
    case $todo in
        "compile")
            LEX=$1
            TXT=$2
            if [ -z $LEX ] && [ -z $TXT ]; then
                usage-txt
            else
                fstcompile --isymbols=$LEX --osymbols=$LEX $TXT > FST.fst
            fi
            ;;
        "printing")
            LEX=$1
            FST=$2
            if [ -z $LEX ] && [ -z $FST ]; then
                usage-fst
            else
                fstprint --isymbols=$LEX --osymbols=$LEX $FST
            fi
            ;;
        "drawing")
            LEX=$1
            FST=$2
            if [ -z $LEX ] && [ -z $FST ]; then
                usage-fst
            else
                fstdraw --isymbols=$LEX --osymbols=$LEX $FST |\
                dot -Tpng > FST.png
            fi
            ;;
        "all")
            LEX=$1
            TXT=$2
            if [ -z $LEX ] && [ -z $TXT ]; then
                usage-txt
            else
                fstcompile --isymbols=$LEX --osymbols=$LEX $TXT > FST.fst
                fstprint --isymbols=$LEX --osymbols=$LEX FST.fst
                fstdraw --isymbols=$LEX --osymbols=$LEX FST.fst |\
                dot -Tpng > FST.png
            fi
            ;;
        "close")
            exit
    esac
done

