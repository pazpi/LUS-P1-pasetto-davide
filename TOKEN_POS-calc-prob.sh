#!/usr/bin/env bash

TOKENCOUNT=$1
POSCOUNT=$2

usage(){
    echo "Usage: $0 POS.count TOKEN.count";
}

if [ -z $POSCOUNT ] && [ -z $TOKENCOUNT ]; then
    usage
else
    while IFS=$' \t\n' read -r token pos count; do
        # printf 'newline\n'
        # printf 'token: %s\tpos: %s\tcount: %s\n' $token $pos $count
        printf -v new_pos '%q' "$pos"
        poscount=$(grep -P "^$new_pos\t" $POSCOUNT | cut -f 2)
        # printf 'count: %s poscount: %s' $count $poscount
        prob=$(echo "scale=5; -l($count / $poscount)" | bc -l)
        echo -e "$token\t$pos\t$prob"
    done < "$TOKENCOUNT"
fi

