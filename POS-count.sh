#!/usr/bin/env bash

INPUT=$1

usage(){
    echo "Usage: $0 file-with-POS-data";
    echo "File format:  token    POS";
}

if [ -z ${INPUT} ]; then
    usage
else
    cat $INPUT | cut -f 2 |    # get second column
    sed '/^ *$/d' |        # remove empty line
    sort | uniq -c |       # uniq list of POS with count
    sed 's/^ *//g' |       # remove leading space
    awk '{OFS="\t"; print $2,$1}'   # swap columns
fi

