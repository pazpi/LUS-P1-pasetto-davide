#!/usr/bin/env bash

INPUT=$1

state=0

num_line=$(< $INPUT wc -l)
prob=$(echo "scale=5; -l(1 / $num_line)"| bc -l )

while IFS=$' \t\n' read -r pos other; do
    echo -e $state'\t'$state'\t<unk>\t'$pos'\t'$prob
done < $INPUT

echo 0
